package main

import (
"fmt"
"sync"
"time"

"github.com/gin-gonic/gin"
)
// this project is based on gin webframework

type keyValue struct {
value string
expiryTime time.Time
conditional string
}

type keyValueStore struct {
mu sync.RWMutex
store map[string]keyValue
queue map[string][]string
}

func main() {
r := gin.Default()
store := keyValueStore{store: make(map[string]keyValue), queue: make(map[string][]string)}

r.POST("/datastore", func(c *gin.Context) {
// structure to for adding , deleting a key-value data store
var cmd struct {
Command string `json:"command"`
Key string `json:"key"`
Value string `json:"value"`
ExpiryTime int `json:"expiry_time,omitempty"`
Conditional string `json:"conditional,omitempty"`
Values []string `json:"values,omitempty"`
}
if err := c.ShouldBindJSON(&cmd); err != nil {
c.JSON(400, gin.H{"error": err.Error()})
return
}

switch cmd.Command {
// this will set the key to the value if key not present
case "SET":
if _, found := store.store[cmd.Key]; !found {
store.store[cmd.Key] = keyValue{value: ""}
}
if err := store.set(cmd.Key, cmd.Value, cmd.ExpiryTime, cmd.Conditional); err != nil {
c.JSON(400, gin.H{"error": err.Error()})
return
}
c.JSON(200, gin.H{"message": "OK"})
// this will get the value corresponding to key which are added if not then error
case "GET":
val, found, err := store.get(cmd.Key)
if err != nil {
c.JSON(400, gin.H{"error": err.Error()})
return
}
if !found {
c.JSON(404, gin.H{"error": "Key not found"})
return
}
c.JSON(200, gin.H{"value": val})
// have'nt used
case "DELETE":
if err := store.delete(cmd.Key); err != nil {
c.JSON(400, gin.H{"error": err.Error()})
return
}
c.JSON(200, gin.H{"message": "OK"})
// implemented on by own
case "FLUSH":
store.flush()
c.JSON(200, gin.H{"message": "OK"})

case "QPUSH":
if err := store.qpush(cmd.Key, cmd.Values...); err != nil {
c.JSON(400, gin.H{"error": err.Error()})
return
}
c.JSON(200, gin.H{"message": "OK"})

case "QPOP":
if val, err := store.qpop(cmd.Key); err != nil {
c.JSON(400, gin.H{"error": err.Error()})
return
} else {
c.JSON(200, gin.H{"value": val})
}

default:
c.JSON(400, gin.H{"error": "Invalid command"})
}
})
// listing and serving on port
r.Run(":8080")
}

// the function set will add the value to the key
// Input : key, value, expiryTime, conditional
// Output : nil or error

func (s *keyValueStore) set(key, value string, expiryTime int, conditional string) error {
s.mu.Lock()
defer s.mu.Unlock()

if conditional != "" {
if _, found := s.store[key]; found && conditional == "NX" {
return fmt.Errorf("key already exists")
}
if _, found := s.store[key]; !found && conditional == "XX" {
return fmt.Errorf("key does not exist")
}
}

if expiryTime > 0 {
s.store[key] = keyValue{value: value, expiryTime: time.Now().Add(time.Duration(expiryTime) * time.Second)}
} else {
s.store[key] = keyValue{value: value}
}

return nil
}

// the function get display the record of given key if key is not presnt then it will send error message
// Input : key
// Output : string, true OR false OR nil or error

func (s *keyValueStore) get(key string) (string, bool, error) {
s.mu.Lock()
defer s.mu.Unlock()

if val, found := s.store[key]; found {
if val.expiryTime.IsZero() || time.Now().Before(val.expiryTime) {
return val.value, true, nil
} else {
delete(s.store, key)
return "", false, nil
}
} else {
return "", false, nil
}

}

// the function delete will be responsible for delete the record present for given pair if not then error message
// Input : key
// Output : nil or error

func (s *keyValueStore) delete(key string) error {
s.mu.Lock()
defer s.mu.Unlock()

if _, found := s.store[key]; !found {
return fmt.Errorf("key not found")
}
delete(s.store, key)

return nil
}

// flush will empty the data structure
// Input : ""
// Output : ""

func (s *keyValueStore) flush() {
s.mu.Lock()
defer s.mu.Unlock()

s.store = make(map[string]keyValue)
s.queue = make(map[string][]string)
}

// the qpush will push values into the given key
// Input : key and values corresponding to that key
// Output : nil or error

func (s *keyValueStore) qpush(key string, values ...string) error {
s.mu.Lock()
defer s.mu.Unlock()

if _, found := s.store[key]; !found {
return fmt.Errorf("key not found")
}

if _, found := s.queue[key]; !found {
s.queue[key] = []string{}
}

s.queue[key] = append(s.queue[key], values...)

return nil
}

// the function qpop will be responsible for retrieving last inserted data in the queue with given key
// if it is not present then it will send error message.
// Input : key
// Output : list of elements or error

func (s *keyValueStore) qpop(key string) (string, error) {
s.mu.Lock()
defer s.mu.Unlock()

if queue, found := s.queue[key]; found {
if len(queue) > 0 {
value := queue[len(queue)-1]
s.queue[key] = queue[:len(queue)-1]
return value, nil
}
return "", fmt.Errorf("queue is empty")
}
return "", fmt.Errorf("queue not found")
}